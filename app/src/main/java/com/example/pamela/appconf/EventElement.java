package com.example.pamela.appconf;

/**
 * Created by pamela on 19/02/2018.
 */

public class EventElement {

    public String titleEvent;
    public String description;
    public String dateStart;
    public String dateEnd;

    public EventElement(){}

    public EventElement(String titleEvent, String dateEnd,String dateStart,String description){
        this.dateEnd=dateEnd;
        this.dateStart=dateStart;
        this.description=description;
        this.titleEvent=titleEvent;
    }

    public void setTitleEvent(String titleEvent){
        this.titleEvent=titleEvent;
    }

    public String getTitleEvent(){
        return this.titleEvent;
    }

    public String getDescription(){
        return this.description;
    }

    public void setDescription(String description){
        this.description=description;
    }

    public void setDateStart(String dateStart){
        this.dateStart=dateStart;
    }

    public String getDateStart(){
        return this.dateStart;
    }

    public void setDateEnd(String dateEnd){
        this.dateEnd=dateEnd;
    }

    public String getDateEnd(){
        return this.dateEnd;
    }

}
