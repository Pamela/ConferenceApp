package com.example.pamela.appconf;

/**
 * Created by pamela on 02/03/2018.
 */

public class Paper_AuthorElement {

    public String affiliation;
    public String email;
    public String first_name;
    public long id;
    public String last_name;
    public String uid;

    public Paper_AuthorElement(){}



    public Paper_AuthorElement(String affiliation, String email, String first_name, long id, String last_name, String uid) {

        this.affiliation = affiliation;
        this.email = email;
        this.first_name = first_name;
        this.id = id;
        this.last_name = last_name;
        this.uid = uid;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFrist_name() {
        return first_name;
    }

    public void setFrist_name(String first_name) {
        this.first_name = first_name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
