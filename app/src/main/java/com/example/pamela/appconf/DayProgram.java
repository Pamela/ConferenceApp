package com.example.pamela.appconf;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by pamela on 20/02/2018.
 */

public class DayProgram extends AppCompatActivity {


    TextView dateView;
    TableLayout tableLayout;
    DatabaseReference mref;

    ArrayList<SessionElement> sessionArray = new ArrayList<>();
    ArrayList<Float> sortTime = new ArrayList<>();

    HashMap<Float,String> timeForViewStart = new HashMap<>();
    HashMap<Float,String> timeForViewEnd = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dayprogram);



        dateView = (TextView) findViewById(R.id.date);
        dateView.setText("Date : "+MainActivity.dayProgram+"/"+MainActivity.monthProgram+"/"+MainActivity.yearProgram);
        dateView.setPaddingRelative(385,40,0,0);

        mref = FirebaseDatabase.getInstance().getReference("sessions");

        mref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    SessionElement sessionElement = ds.getValue(SessionElement.class);
                    sessionArray.add(sessionElement);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        tableLayout = (TableLayout)findViewById(R.id.mTableLayout);

        String start = MainActivity.sessionOfDay.get(0).getStartTime();
        start = start.substring(11, 16);
        String end = MainActivity.sessionOfDay.get(0).getEndTime();
        end = end.substring(11, 16);
        String endArray[] = end.split(":");
        String startArray[] = start.split(":");
        String floatStartTimeString = startArray[0] + startArray[1];
        String floatEndTimeString = endArray[0] + endArray[1];

        float sumTimeS = Float.parseFloat(floatStartTimeString);
        float sumTimeE = Float.parseFloat(floatEndTimeString);
        sortTime.add(sumTimeS);
        sortTime.add(sumTimeE);
        timeForViewStart.put(sumTimeS, start);
        timeForViewEnd.put(sumTimeE, end);


        for (int i = 0 ; i<MainActivity.sessionOfDay.size(); i++){
            String starti = MainActivity.sessionOfDay.get(i).getStartTime();
            starti = starti.substring(11,16);
            String endi = MainActivity.sessionOfDay.get(i).getEndTime();
            endi = endi.substring(11,16);
            String startArrayi[] = starti.split(":");
            String endArrayi[] = endi.split(":");

            String floatStartTimeStringI = startArrayi[0] + startArrayi[1];
            String floatEndTimeStringI = endArrayi[0] + endArrayi[1];

            float sumTimeiS = Float.parseFloat(floatStartTimeStringI);
            float sumTimeiE = Float.parseFloat(floatEndTimeStringI);
            if (!sortTime.contains(sumTimeiS)) {
                sortTime.add(sumTimeiS);
                timeForViewStart.put(sumTimeiS, starti);}
            if (!sortTime.contains(sumTimeiE)) {
                sortTime.add(sumTimeiE);
                timeForViewEnd.put(sumTimeiE, endi);
            }
        }
        Collections.sort(sortTime);

        TableRow tableRows[] = new TableRow[sortTime.size()];
        TextView textViews[] = new TextView[MainActivity.sessionOfDay.size()];
        TextView textViewsE[] = new TextView[sortTime.size()*MainActivity.sessionOfDay.size()+MainActivity.sessionOfDay.size()];
        TextView textViewsS[] = new TextView[sortTime.size()*MainActivity.sessionOfDay.size()+MainActivity.sessionOfDay.size()];
        TextView textViewsEmpty[] = new TextView[sortTime.size()*MainActivity.sessionOfDay.size()+MainActivity.sessionOfDay.size()];
        TextView textViewsColor[] = new TextView[sortTime.size()*MainActivity.sessionOfDay.size()+MainActivity.sessionOfDay.size()];

        TableRow tableRow = new TableRow(this);

        TextView textView = new TextView(this);
        textView.setText("     ");
        tableRow.addView(textView);

        for (int t = 0; t<sortTime.size();t++) {
            if (timeForViewStart.containsKey(sortTime.get(t))) {
                TextView textViewH = new TextView(this);
                textViewH.setText(timeForViewStart.get(sortTime.get(t)) + "     ");
                tableRow.addView(textViewH);
            } else if (timeForViewEnd.containsKey(sortTime.get(t))) {
                TextView textViewH = new TextView(this);
                textViewH.setText(timeForViewEnd.get(sortTime.get(t)) + "     ");
                tableRow.addView(textViewH);
            }

        }

        tableLayout.addView(tableRow);


        for (int i = 0 ; i < MainActivity.sessionOfDay.size(); i++){

            String color;

            if(i%2==0){
                color = "#f0f8ff";
            }else{
                color="#ffebcd";
            }

            tableRows[i] = new TableRow(this);

            textViews[i] = new TextView(this);
            textViews[i].setText(MainActivity.sessionOfDay.get(i).getTitle());
            textViews[i].setWidth(300);
            textViews[i].setBackgroundColor(Color.parseColor(color));
            final int finalI = i;
            textViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DayProgram.this, ShowTracks.class);
                    intent.putExtra("uid", MainActivity.sessionOfDay.get(finalI).getUid());
                    startActivity(intent);
                }
            });
            tableRows[i].addView(textViews[i]);

            String starti = MainActivity.sessionOfDay.get(i).getStartTime();
            String endi = MainActivity.sessionOfDay.get(i).getEndTime();
            endi = endi.substring(11, 16);
            starti = starti.substring(11, 16);
            String startArrayi[] = starti.split(":");
            String endArrayi[] = endi.split(":");

            String fstartStringi = startArrayi[0] + startArrayi[1];
            String fendStringi = endArrayi[0] + endArrayi[1];

            float fstarti = Float.parseFloat(fstartStringi);
            float fendi = Float.parseFloat(fendStringi);



            for (int s = 0; s<sortTime.size(); s++){

                if (fstarti == sortTime.get(s)){
                    textViewsS[s+i] = new TextView(this);
                    textViewsS[s+i].setText("Start : "+MainActivity.sessionOfDay.get(i).getTitle());
                    textViewsS[s+i].setWidth(300);
                    textViewsS[s+i].setHeight(200);
                    textViewsS[s+i].setBackgroundColor(Color.parseColor(color));
                    textViewsS[s+i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(DayProgram.this, ShowTracks.class);
                            intent.putExtra("uid", MainActivity.sessionOfDay.get(finalI).getUid());
                            startActivity(intent);
                        }
                    });
                    tableRows[i].addView(textViewsS[s+i]);
                }else if(fendi == sortTime.get(s)){
                    textViewsE[s+i] = new TextView(this);
                    textViewsE[s+i].setText("End : "+MainActivity.sessionOfDay.get(i).getTitle());
                    textViewsE[s+i].setWidth(300);
                    textViewsE[s+i].setHeight(200);
                    textViewsE[s+i].setBackgroundColor(Color.parseColor(color));
                    textViewsE[s+i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(DayProgram.this, ShowTracks.class);
                            intent.putExtra("uid", MainActivity.sessionOfDay.get(finalI).getUid());
                            startActivity(intent);
                        }
                    });
                    tableRows[i].addView(textViewsE[s+i]);

                }else if( fstarti<sortTime.get(s) && fendi>sortTime.get(s)){
                    textViewsColor[s+i] = new TextView(this);
                    textViewsColor[s+i].setText(" ");
                    textViewsColor[s+i].setWidth(300);
                    textViewsColor[s+i].setHeight(200);
                    textViewsColor[s+i].setBackgroundColor(Color.parseColor(color));
                    textViewsColor[s+i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(DayProgram.this, ShowTracks.class);
                            intent.putExtra("uid", MainActivity.sessionOfDay.get(finalI).getUid());
                            startActivity(intent);
                        }
                    });
                    tableRows[i].addView(textViewsColor[s+i]);
                }else{
                    textViewsEmpty[s+i] = new TextView(this);
                    textViewsEmpty[s+i].setText(" ");
                    textViewsEmpty[s+i].setWidth(200);
                    tableRows[i].addView(textViewsEmpty[s+i]);
                }

            }

            tableLayout.addView(tableRows[i]);
        }

        TableRow tableRowButtom = new TableRow(this);

        TextView textViewButtom = new TextView(this);
        textViewButtom.setText("     ");
        tableRowButtom.addView(textViewButtom);

        for (int t = 0; t<sortTime.size();t++) {
            if (timeForViewStart.containsKey(sortTime.get(t))) {
                TextView textViewHButtom = new TextView(this);
                textViewHButtom.setText(timeForViewStart.get(sortTime.get(t)) + "     ");
                tableRowButtom.addView(textViewHButtom);
            } else if (timeForViewEnd.containsKey(sortTime.get(t))) {
                TextView textViewHButtom = new TextView(this);
                textViewHButtom.setText(timeForViewEnd.get(sortTime.get(t)) + "     ");
                tableRowButtom.addView(textViewHButtom);
            }

        }

        tableLayout.addView(tableRowButtom);

        Button btn = (Button) findViewById(R.id.btnBack);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DayProgram.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

}
