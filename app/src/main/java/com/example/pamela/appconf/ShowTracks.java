package com.example.pamela.appconf;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by pamela on 27/02/2018.
 */

public class ShowTracks extends AppCompatActivity{

    LinearLayout linearLayout;

    DatabaseReference reftrack;
    DatabaseReference refSessiontrack;

    FirebaseDatabase database;
    Uri fileUri;
    private FirebaseStorage storage;
    private AlertDialog.Builder alert;
    Intent mintentArray[];

    private ArrayList<TrackElement> tracksArray= new ArrayList<>();

    private ArrayList<String> uidSessionTracksArray = new ArrayList<>();

    private String startDate, endDate, title;

    ContentResolver contentResolver;

    private String uidSession;
    private SessionFollowList sessionFollowList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_tracks);

        linearLayout = (LinearLayout)findViewById(R.id.linear);

        contentResolver = ShowTracks.this.getContentResolver();


        final int callbackId = 42;
        checkPermissions(callbackId, Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR);
        Intent intent = getIntent();
        uidSession = intent.getStringExtra("uid");

        verifyStoragePermissions(this);
        alert = new AlertDialog.Builder(this);
        //Log.d("PROVA TRACKS ACTIVITY", ""+uidSession);
        database = FirebaseDatabase.getInstance();

        refSessiontrack = database.getReference("sessions");


        mintentArray = new Intent[MainActivity.sessionOfDay.size()-1];


        refSessiontrack.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    String uidS = (String) ds.child("uid").getValue();
                    if(uidS.equals(uidSession)){

                        int countTracks = (int) ds.child("tracks").getChildrenCount();
                        for (int i = 0 ; i<countTracks; i++){
                            String si = String.valueOf(i);
                            String uidSessionTrack = (String) ds.child("tracks").child(si).getValue();
                            //Log.d("PROVA", "uid:"+uidSessionTrack);
                            uidSessionTracksArray.add(uidSessionTrack);
                        }


                    }
                }
                int size = uidSessionTracksArray.size();
                String sizeString = String.valueOf(size);
                Log.d("Prova1", "size()"+sizeString);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });

        TextView textViewPresentation[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewCreated[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewType[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewDescription[] = new TextView[MainActivity.presentationSessionOfDay.size()];

        TextView textViewFirstName[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewLastName[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewEmail[] = new TextView[MainActivity.presentationSessionOfDay.size()];
        TextView textViewAffiliation[] = new TextView[MainActivity.presentationSessionOfDay.size()];

        TextView textViewTitleP[] = new TextView[MainActivity.presentationSessionOfDay.size()];

        reftrack = database.getReference("tracks");

        reftrack.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = uidSessionTracksArray.size();
                String sizeString = String.valueOf(size);
                Log.d("Prova2", "size()"+sizeString);
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    String uidTrack = (String) ds.child("uid").getValue();
                    if(uidSessionTracksArray.contains(uidTrack)){

                        TrackElement trackElement = ds.getValue(TrackElement.class);
                        tracksArray.add(trackElement);
                        //Log.d("Prova3", "element track:"+(String)tracksArray.get(0).getTitle());
                    }
                }

                showTracks();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });

        for (int j = 0 ; j <MainActivity.sessionOfDay.size(); j++){
            if (MainActivity.sessionOfDay.get(j).getUid().equals(uidSession)) {
                startDate = (String) MainActivity.sessionOfDay.get(j).getStartTime();
                title = (String) MainActivity.sessionOfDay.get(j).getTitle();
                endDate = (String) MainActivity.sessionOfDay.get(j).getEndTime();
                TextView textViewTitleS = new TextView(this);
                textViewTitleS.setGravity(Gravity.CENTER_HORIZONTAL);
                textViewTitleS.setTextColor(Color.parseColor("#dc143c"));
                textViewTitleS.setTextSize(30);
                textViewTitleS.setText(MainActivity.sessionOfDay.get(j).getTitle());
                linearLayout.addView(textViewTitleS);
                if (!MainActivity.sessionOfDay.get(j).getDescription().equals("")) {
                    TextView textViewDescriptionS = new TextView(this);
                    textViewDescriptionS.setTextSize(15);
                    textViewDescriptionS.setText("Description: " + MainActivity.sessionOfDay.get(j).getDescription());
                    linearLayout.addView(textViewDescriptionS);
                }
                if (!MainActivity.sessionOfDay.get(j).getSpeaker().equals("")) {
                    TextView textViewSpeakersS = new TextView(this);
                    textViewSpeakersS.setTextSize(15);
                    textViewSpeakersS.setText("Speakers: " + MainActivity.sessionOfDay.get(j).getSpeaker());
                    linearLayout.addView(textViewSpeakersS);
                }

                if (MainActivity.presentationSessionOfDay.size() > 0) {

                    TextView text = new TextView(this);
                    text.setText("Presentations: ");
                    linearLayout.addView(text);

                    for (int p = 0; p < MainActivity.presentationSessionOfDay.size(); p++) {

                        if (MainActivity.presentationSessionOfDay.get(p).getSession().equals(MainActivity.sessionOfDay.get(j).getUid())) {
                            textViewPresentation[p] = new TextView(this);
                            textViewPresentation[p].setTextColor(Color.parseColor("#dc143c"));
                            textViewPresentation[p].setTextSize(21);
                            textViewPresentation[p].setText("Title: " + MainActivity.presentationSessionOfDay.get(p).getTitle());
                            linearLayout.addView(textViewPresentation[p]);

                            textViewDescription[p] = new TextView(this);
                            textViewDescription[p].setTextSize(15);
                            textViewDescription[p].setText("Decription: " + MainActivity.presentationSessionOfDay.get(p).getDescription());
                            linearLayout.addView(textViewDescription[p]);

                            String createdDate = MainActivity.presentationSessionOfDay.get(p).getCreated();
                            createdDate = createdDate.substring(0, 10);
                            String createdHours = MainActivity.presentationSessionOfDay.get(p).getCreated();
                            createdHours = createdHours.substring(11, 16);

                            textViewCreated[p] = new TextView(this);
                            textViewCreated[p].setTextSize(15);
                            textViewCreated[p].setText("Created: " + createdDate+" - "+createdHours );
                            linearLayout.addView(textViewCreated[p]);

                            textViewType[p] = new TextView(this);
                            textViewType[p].setTextSize(15);
                            textViewType[p].setText("Type: " + MainActivity.presentationSessionOfDay.get(p).getType());
                            linearLayout.addView(textViewType[p]);

                            TextView textViewPA = new TextView(this);
                            textViewPA.setTextSize(21);
                            textViewPA.setText("Author information: ");
                            linearLayout.addView(textViewPA);

                            textViewFirstName[p] = new TextView(this);
                            textViewFirstName[p].setTextSize(15);
                            textViewFirstName[p].setText("First name: " + MainActivity.presentationSessionOfDay.get(p).getPaper_author().getFrist_name());
                            linearLayout.addView(textViewFirstName[p]);

                            textViewLastName[p] = new TextView(this);
                            textViewLastName[p].setTextSize(15);
                            textViewLastName[p].setText("Last name: " + MainActivity.presentationSessionOfDay.get(p).getPaper_author().getLast_name());
                            linearLayout.addView(textViewLastName[p]);

                            textViewEmail[p] = new TextView(this);
                            textViewEmail[p].setTextSize(15);
                            textViewEmail[p].setText("Email: " + MainActivity.presentationSessionOfDay.get(p).getPaper_author().getEmail());
                            linearLayout.addView(textViewEmail[p]);

                            textViewAffiliation[p] = new TextView(this);
                            textViewAffiliation[p].setTextSize(15);
                            textViewAffiliation[p].setText("Affiliation: " + MainActivity.presentationSessionOfDay.get(p).getPaper_author().getAffiliation());
                            linearLayout.addView(textViewAffiliation[p]);

                            TextView textViewP = new TextView(this);
                            textViewP.setTextSize(21);
                            textViewP.setText("Paper information: ");
                            linearLayout.addView(textViewP);

                            textViewTitleP[p] = new TextView(this);
                            textViewTitleP[p].setTextSize(15);
                            textViewTitleP[p].setText("Title: " + MainActivity.presentationSessionOfDay.get(p).getPaper().getTitle());
                            linearLayout.addView(textViewTitleP[p]);
                            long id = MainActivity.presentationSessionOfDay.get(p).getPaper().getId();
                            final String ids = String.valueOf(id);
                            Button btndowload = new Button(this);
                            btndowload.setText("Download");
                            btndowload.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    updateFile(ids);
                                }
                            });
                            linearLayout.addView(btndowload);

                        }
                    }

                }
            }
        }

        sessionFollowList = new SessionFollowList(title,uidSession);



    }



    private void showTracks() {
        if(tracksArray.size()>0) {
            TextView textView = new TextView(ShowTracks.this);
            textView.setTextSize(15);
            textView.setText("Tracks");
            linearLayout.addView(textView);

            for (int i = 0; i < tracksArray.size(); i++) {
                TextView textViewTitle = new TextView(ShowTracks.this);
                textViewTitle.setGravity(Gravity.CENTER_HORIZONTAL);
                textViewTitle.setTextColor(Color.parseColor("#dc143c"));
                textViewTitle.setTextSize(21);
                textViewTitle.setText(tracksArray.get(i).getTitle());
                linearLayout.addView(textViewTitle);

                TextView textViewDescription = new TextView(ShowTracks.this);
                textViewDescription.setTextSize(15);
                textViewDescription.setText("Description: " + tracksArray.get(i).getDescription());
                linearLayout.addView(textViewDescription);

            }
        }

        Button btnFollow = new Button(ShowTracks.this);
        btnFollow.setText("Follow");
        btnFollow.setGravity(Gravity.CENTER_HORIZONTAL);
        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = startDate.substring(0,10);
                String hms = startDate.substring(11,16);

                String dateSlplit[]= date.split("-");
                String hmsSplit[] = hms.split(":");

                int year = Integer.parseInt(dateSlplit[0]);
                int month = Integer.parseInt(dateSlplit[1]);
                int day = Integer.parseInt(dateSlplit[2]);

                int h = Integer.parseInt(hmsSplit[0]);
                int m = Integer.parseInt(hmsSplit[1]);

                Calendar calendar = Calendar.getInstance();
                Calendar now = Calendar.getInstance();

                calendar.set(year,month-1,day,h,m);
                String time = calendar.getTime().toString();
                //Log.d("TIME", ""+time);
                if (calendar.compareTo(now)<=0){
                    Toast.makeText(ShowTracks.this, "This event has already been", Toast.LENGTH_LONG).show();
                }else if(SessionFollowList.sessionFollow.contains(sessionFollowList)) {
                    Toast.makeText(ShowTracks.this, "follow this session already", Toast.LENGTH_LONG).show();
                }else{
                    SessionFollowList.sessionFollow.add(sessionFollowList);
                    Toast.makeText(ShowTracks.this,"you following this session",Toast.LENGTH_LONG).show();
                    int id = SessionFollowList.sessionFollow.size()-1;
                    Calendar calendarStart = new GregorianCalendar();
                    calendarStart.set(year,month-1,day,h,m-10);

                    Calendar calendarEnd = new GregorianCalendar();
                    calendarEnd.set(year,month-1,day,h,m);

                    ContentValues contentValues = new ContentValues();

                    contentValues.put(CalendarContract.Events.TITLE, sessionFollowList.getTitle());
                    contentValues.put(CalendarContract.Events.DESCRIPTION, "this session will start in 10 minutes\n");
                    contentValues.put(CalendarContract.Events.DTSTART, calendarStart.getTimeInMillis());
                    contentValues.put(CalendarContract.Events.DTEND, calendarEnd.getTimeInMillis());
                    contentValues.put(CalendarContract.Events.CALENDAR_ID, 1);
                    contentValues.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
                    contentValues.put(CalendarContract.Events.HAS_ALARM,1);

                    Uri uri = contentResolver.insert(CalendarContract.Events.CONTENT_URI,contentValues);
                    SessionFollowList.eventUriArray.add(uri);
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setData(uri);
                    startActivity(intent);

                }
            }
        });

        linearLayout.addView(btnFollow);
        Button btnUnfollow = new Button(ShowTracks.this);
        btnUnfollow.setGravity(Gravity.CENTER_HORIZONTAL);
        btnUnfollow.setText("Unfollow");
        btnUnfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(SessionFollowList.sessionFollow.contains(sessionFollowList)){
                    for(int i =0 ; i<SessionFollowList.sessionFollow.size(); i++){
                        if(SessionFollowList.sessionFollow.get(i).getUid().equals(uidSession)){
                            int delete=0;
                            delete = deleteCalendarEntry(SessionFollowList.eventUriArray.get(i));
                            if (delete!=0){
                                Toast.makeText(ShowTracks.this,"Deleted",Toast.LENGTH_LONG).show();
                                SessionFollowList.sessionFollow.remove(i);

                            }
                        }
                    }
                }else {
                    Toast.makeText(ShowTracks.this,"Don't follow this session",Toast.LENGTH_LONG).show();
                }

            }
        });
        linearLayout.addView(btnUnfollow);

      }


    private int deleteCalendarEntry(Uri eventUri) {
        int iNumRowsDeleted = 0;

        iNumRowsDeleted = getContentResolver().delete(eventUri, null, null);
        //String printNumRows = String.valueOf(iNumRowsDeleted);
        //Log.d("DELETE CALENDAR EVENT", " calendar entry:"+printNumRows);

        return iNumRowsDeleted;
    }

    private void checkPermissions(int callbackId, String ...permissionsId) {
        boolean permissions = true;
        for (String p : permissionsId) {
            permissions = permissions && ContextCompat.checkSelfPermission(this, p) == PERMISSION_GRANTED;
        }

        if (!permissions)
            ActivityCompat.requestPermissions(this, permissionsId, callbackId);
    }

    public void updateFile(final String id){
        //Log.d("TRY", "trying update");
        storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        StorageReference ref = storageRef.child(id+".pdf");
        ref.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long updTime = storageMetadata.getUpdatedTimeMillis();
                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                String nof = "";
                File[] list = dir.listFiles();
                for (File f : list) {
                    if (f.getName().contains(id) && f.getName().contains(".pdf")) {
                        nof = f.getName();
                        //Log.d("FILE", "nof: " + nof);
                    }
                }
                File file = new File(dir, nof);
                if(nof.equals("")){
                    downloadFile(id);
                }
                //Log.d("FILE NAME", file.getName());
                if (file.exists()) {
                    //Log.d("FILE", "File exists");
                    if (file.lastModified() < updTime) {
                        file.delete();
                        Toast.makeText(ShowTracks.this, "File is up to date", Toast.LENGTH_LONG);
                        alert.setTitle("File is outdated");
                        alert.setMessage("Press OK if you want update your "+id+" file.");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                dialogInterface.dismiss();
                                downloadFile(id);
                            }
                        });
                        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                dialogInterface.dismiss();
                            }
                        });
                        alert.create();
                        alert.show();
                    }
                }
                else
                    downloadFile(id);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void downloadFile(String id){
        storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        final StorageReference ref = storageRef.child(id+".pdf");
        try {


            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            final File localFile = File.createTempFile("conference", ".json", folder);
            // Log.d("TAG", "Prova download file");
            ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(getApplicationContext(), "Download completed", Toast.LENGTH_LONG).show();
                    // Log.d("TAG","Download completato");

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_LONG).show();
                    //  Log.d("TAG","Download fallito");
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileUri = data.getData();
    }





    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }


    }
