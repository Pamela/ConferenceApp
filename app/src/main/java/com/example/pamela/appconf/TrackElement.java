package com.example.pamela.appconf;

/**
 * Created by pamela on 22/02/2018.
 */

public class TrackElement {

    public String title;
    public String description;
    public String event;
    public String uid;

    public void TrackElement(){}

    public void TrackElement(String title, String description, String event, String uid){
        this.title=title;
        this.description=description;
        this.event=event;
        this.uid=uid;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setDescription(String description){
        this.description=description;
    }

    public String getDescription(){
        return this.description;
    }

    public void setEvent(String event){
        this.event=event;
    }

    public String getEvent(){
        return this.event;
    }

    public void setUid(String uid){
        this.uid=uid;
    }

    public String getUid(){
        return this.uid;
    }
}
