package com.example.pamela.appconf;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pamela on 20/02/2018.
 */

public class SessionElement {

    public String description;
    public String endTime;
    public String speaker;
    public String startTime;
    public String title;
    public ArrayList<String> tracks = new ArrayList<>();
    public String uid;

    public static HashMap<String, String> colorSession = new HashMap<>();

    public SessionElement(){}

    public SessionElement(String description, String endTime, String speaker, String startTime, String title, String uid, ArrayList<String> tracks){
        this.description=description;
        this.endTime=endTime;
        this.speaker=speaker;
        this.startTime=startTime;
        this.title=title;
        this.uid=uid;
        this.tracks=tracks;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return this.description;
    }

    public void setEndTime(String endTime){
        this.endTime = endTime;
    }

    public String getEndTime(){
        return this.endTime;
    }

    public void setSpeaker(String speaker){
        this.speaker = speaker;
    }

    public String getSpeaker(){
        return this.speaker;
    }

    public void setStartTime(String startTime){
        this.startTime = startTime;
    }

    public String getStartTime(){
        return this.startTime;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){

        return this.title;
    }

    public ArrayList<String> getTracks(){
        return this.tracks;
    }

    public void setTracks(ArrayList<String> tracks){
        this.tracks=tracks;
    }

    public void setUid(String uid){
        this.uid = uid;
    }

    public String getUid(){
        return this.uid;
    }

}
