package com.example.pamela.appconf;

/**
 * Created by pamela on 02/03/2018.
 */

public class PaperElement {
    public String aabstract;
    public String kywords;
    public long id;
    public String manuscript_download_url;
    public String manuscript_filename;
    public String manuscript_type;
    public String title;
    public String uid;

    public PaperElement() {}

    public String getAabstract() {
        return aabstract;
    }

    public void setAabstract(String aabstract) {
        this.aabstract = aabstract;
    }

    public String getKywords() {
        return kywords;
    }

    public void setKywords(String kywords) {
        this.kywords = kywords;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getManuscript_download_url() {
        return manuscript_download_url;
    }

    public void setManuscript_download_url(String manuscript_download_url) {
        this.manuscript_download_url = manuscript_download_url;
    }

    public String getManuscript_filename() {
        return manuscript_filename;
    }

    public void setManuscript_filename(String manuscript_filename) {
        this.manuscript_filename = manuscript_filename;
    }

    public String getManuscript_type() {
        return manuscript_type;
    }

    public void setManuscript_type(String manuscript_type) {
        this.manuscript_type = manuscript_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
