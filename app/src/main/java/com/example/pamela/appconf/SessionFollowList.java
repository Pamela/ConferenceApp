package com.example.pamela.appconf;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by pamela on 28/02/2018.
 */

public class SessionFollowList {

    public String title;
    public String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public static ArrayList<SessionFollowList> sessionFollow = new ArrayList<>();
    public static ArrayList<Uri> eventUriArray = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public SessionFollowList(String title, String uid){
        this.title=title;
        this.uid = uid;
    }
}
