package com.example.pamela.appconf;

import java.util.ArrayList;

/**
 * Created by pamela on 02/03/2018.
 */

public class PresentationElement {

    public String created;
    public String description;
    public long id;
    public String title;
    public String type;
    public String uid;

    public String session;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public PaperElement paper;
    public Paper_AuthorElement paper_author;

    public PaperElement getPaper() {
        return paper;
    }

    public void setPaper(PaperElement paper) {
        this.paper = paper;
    }

    public Paper_AuthorElement getPaper_author() {
        return paper_author;
    }

    public void setPaper_author(Paper_AuthorElement paper_author) {
        this.paper_author = paper_author;
    }

    public void PresentationElement(){}

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
