package com.example.pamela.appconf;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private CalendarView mCalendar;

    private int dayS,monthS,yearS,dayE,monthE,yearE;
    private int daySession,monthSession,yearSession;

    TextView titleView, dateSView, dateEView;

    DatabaseReference ref;
    DatabaseReference refsession;

    FirebaseDatabase database;

    private AlertDialog.Builder alert;
    private FirebaseStorage storage;

    Uri fileUri;

    public static int dayProgram, monthProgram, yearProgram;
    public static ArrayList<SessionElement> sessionOfDay ;
    public static ArrayList<PresentationElement> presentationSessionOfDay ;

    private ArrayList<SessionElement> sessionArray = new ArrayList<>();
    private ArrayList<PresentationElement> presentationArray = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        verifyStoragePermissions(this);



        titleView = (TextView)findViewById(R.id.titleEvent);
        dateSView = (TextView)findViewById(R.id.dateStart);
        dateEView = (TextView)findViewById(R.id.dateEnd);
        mCalendar=(CalendarView) findViewById(R.id.calendarView);


        database = FirebaseDatabase.getInstance();
        alert = new AlertDialog.Builder(this);

        updateFile();

        ref = database.getReference("event");

        sessionOfDay = new ArrayList<>();
        presentationSessionOfDay = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                titleView.setText((String) dataSnapshot.child("title").getValue());
                String dateStart =(String) dataSnapshot.child("startDate").getValue();
                String dateEnd =(String) dataSnapshot.child("endDate").getValue();

                dateStart = dateStart.substring(0,10);
                dateEnd = dateEnd.substring(0,10);

                dateSView.setText(dateStart);
                dateEView.setText(dateEnd);

                String dateS[] = dateStart.split("-");
                String dateE[] = dateEnd.split("-");


                yearS = Integer.parseInt(dateS[0]);
                monthS = Integer.parseInt(dateS[1]);
                dayS = Integer.parseInt(dateS[2]);

                yearE = Integer.parseInt(dateE[0]);
                monthE = Integer.parseInt(dateE[1]);
                dayE = Integer.parseInt(dateE[2]);

                monthS = monthS-1;

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, yearS);
                calendar.set(Calendar.MONTH, monthS);
                calendar.set(Calendar.DAY_OF_MONTH, dayS);

                long mTime = calendar.getTimeInMillis();

                mCalendar.setDate(mTime);





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });

        refsession = database.getReference("sessions");
        refsession.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    SessionElement sessionE = ds.getValue(SessionElement.class);
                    sessionArray.add(sessionE);
                    DataSnapshot dsPresentation = ds.child("presentations");
                    for (DataSnapshot ds1 : dsPresentation.getChildren()) {
                        PresentationElement presentation = new PresentationElement();
                        String session = (String) sessionE.getUid();
                        String created = (String) ds1.child("created").getValue();
                        String description = (String) ds1.child("description").getValue();
                        long id = (long) ds1.child("id").getValue();
                        String title = (String) ds1.child("title").getValue();
                        String type = (String) ds1.child("type").getValue();
                        String uid = (String) ds1.child("uid").getValue();

                        PaperElement paper = ds1.child("paper").getValue(PaperElement.class);
                        Paper_AuthorElement paper_author = ds1.child("paper_author").getValue(Paper_AuthorElement.class);

                        presentation.setCreated(created);
                        presentation.setDescription(description);
                        presentation.setId(id);
                        presentation.setPaper(paper);
                        presentation.setPaper_author(paper_author);
                        presentation.setTitle(title);
                        presentation.setType(type);
                        presentation.setUid(uid);
                        presentation.setSession(session);

                        presentationArray.add(presentation);

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });

        mCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                if(year==yearS || year==yearE){
                    if (month==monthS || month == monthE || month>monthS && month<monthE){
                        if (dayOfMonth==dayS || dayOfMonth == dayE || dayOfMonth>dayS && dayOfMonth<dayE ){
                            session(year,month,dayOfMonth);
                            //Log.d("Prova", "giorno selezionato "+dayOfMonth);
                        }else {
                            Toast.makeText(MainActivity.this, " no scheduled session ", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(MainActivity.this, " no scheduled session ", Toast.LENGTH_LONG).show();
                    }

                }else {
                    Toast.makeText(MainActivity.this, " no scheduled session ", Toast.LENGTH_LONG).show();
                }

            }
        });





    }







    private void session(int year, int month, int dayOfMonth) {
        int check = 0;
        for(int i=0; i<sessionArray.size();i++) {

            String dateSession = sessionArray.get(i).getStartTime();
            dateSession = dateSession.substring(0, 10);
            String dateS[] = dateSession.split("-");
            yearSession = Integer.parseInt(dateS[0]);
            monthSession = Integer.parseInt(dateS[1]);
            monthSession=monthSession-1;
            daySession = Integer.parseInt(dateS[2]);

            if (yearSession==year){
                if (monthSession==month){
                    if (daySession==dayOfMonth){
                        if(check != 1) {
                            dayProgram = dayOfMonth;
                            monthProgram = month+1;
                            yearProgram = year;
                            check=1;
                        }
                        String description = sessionArray.get(i).getDescription();
                        String endTime = sessionArray.get(i).getEndTime();
                        String speaker = sessionArray.get(i).getSpeaker();
                        String startTime = sessionArray.get(i).getStartTime();
                        String titleS = sessionArray.get(i).getTitle();
                        String uid = sessionArray.get(i).getUid();
                        ArrayList<String> tracks = sessionArray.get(i).getTracks();

                        SessionElement session = new SessionElement(description,endTime,speaker,startTime,titleS,uid,tracks);
                        sessionOfDay.add(session);
                        for (int p = 0; p<presentationArray.size(); p++) {
                            if (presentationArray.get(p).getSession().equals(uid)) {
                                presentationSessionOfDay.add(presentationArray.get(p));
                            }
                        }

                    }
                }
            }
        }

        Intent intent = new Intent(MainActivity.this, DayProgram.class);
        startActivity(intent);

    }

    public void updateFile(){
        //Log.d("TRY", "trying update");
        storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        StorageReference ref = storageRef.child("conference.json");
        ref.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long updTime = storageMetadata.getUpdatedTimeMillis();
                File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                String nof = "";
                File[] list = dir.listFiles();
                for (File f : list) {
                    if (f.getName().contains("conference") && f.getName().contains(".json")) {
                        nof = f.getName();
                        //Log.d("FILE", "nof: " + nof);
                    }
                }
                File file = new File(dir, nof);
                if(nof.equals("")){
                    downloadFile();
                }
                //Log.d("FILE NAME", file.getName());
                if (file.exists()) {
                    //Log.d("FILE", "File exists");
                    if (file.lastModified() < updTime) {
                        file.delete();
                        Toast.makeText(MainActivity.this, "File is up to date", Toast.LENGTH_LONG);
                        alert.setTitle("File is outdated");
                        alert.setMessage("Press OK if you want update your conference file.");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                dialogInterface.dismiss();
                                downloadFile();
                            }
                        });
                        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                                dialogInterface.dismiss();
                            }
                        });
                        alert.create();
                        alert.show();
                    }
                }
                else
                    downloadFile();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void downloadFile(){
        storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        final StorageReference ref = storageRef.child("conference.json");
        try {


            File folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            final File localFile = File.createTempFile("conference", ".json", folder);
           // Log.d("TAG", "Prova download file");
            ref.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(getApplicationContext(), "Download completed", Toast.LENGTH_LONG).show();
                   // Log.d("TAG","Download completato");

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Download failed", Toast.LENGTH_LONG).show();
                  //  Log.d("TAG","Download fallito");
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileUri = data.getData();
    }





    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

}
